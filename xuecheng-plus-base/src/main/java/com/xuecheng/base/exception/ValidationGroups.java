package com.xuecheng.base.exception;

/**
 * @author: lzy
 * @create: 2024-03-15 16:11
 * @Description: 用于分组校验，定义一些常用的组
 * @version: v1.0
 */
public class ValidationGroups {

    public interface Insert{};
    public interface Update{};
    public interface Delete{};

}
