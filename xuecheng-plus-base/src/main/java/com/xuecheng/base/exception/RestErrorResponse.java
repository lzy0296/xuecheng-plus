package com.xuecheng.base.exception;

import java.io.Serializable;

/**
 * @author: lzy
 * @create: 2024-03-15 11:47
 * @Description: 和前端约定返回的异常信息模型
 * @version: v1.0
 */
public class RestErrorResponse implements Serializable {

    private String errMessage;

    public RestErrorResponse(String errMessage){
        this.errMessage= errMessage;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }

}
