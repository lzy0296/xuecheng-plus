package com.xuecheng.media.model.dto;

import com.xuecheng.media.model.po.MediaFiles;
import lombok.Data;
import lombok.ToString;

/**
 * @author: lzy
 * @create: 2024-03-18 15:54
 * @Description: 上传图片返回的对象
 * @version: v1.0
 */
@Data
@ToString
public class UploadFileResultDto extends MediaFiles {
}
