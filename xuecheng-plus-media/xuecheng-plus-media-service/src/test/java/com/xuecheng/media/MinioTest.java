package com.xuecheng.media;

import com.j256.simplemagic.ContentInfo;
import com.j256.simplemagic.ContentInfoUtil;
import io.minio.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author: lzy
 * @create: 2024-03-18 12:04
 * @Description: 测试minio的sdk
 * @version: v1.0
 */
@SpringBootTest
public class MinioTest {

    MinioClient minioClient =
            MinioClient.builder()
                    .endpoint("http://192.168.101.65:9000")
                    .credentials("minioadmin", "minioadmin")
                    .build();

    //上传文件
    @Test
    public void test_upload() throws Exception {
        //通过扩展名得到媒体资源类型mimeType
        //通过扩展名取出mimeType
        ContentInfo mimeTypeMatch = ContentInfoUtil.findMimeTypeMatch(".jpg");
        String mimeType = MediaType.APPLICATION_OCTET_STREAM_VALUE;//通过mimeType，字节流
        if(mimeTypeMatch!=null){
            mimeType = mimeTypeMatch.getMimeType();
        }

        //上传文件的参数信息
        UploadObjectArgs uploadObjectArgs = UploadObjectArgs.builder()
                .bucket("testbucker") //确定桶
                .filename("E:\\图片\\壁纸\\壁纸.jpg") //指定本地文件路径
                .object("壁纸.jpg") //对象名
                .contentType(mimeType) //设置媒体文件类型
                .build();
        //上传文件
        minioClient.uploadObject(uploadObjectArgs);
    }

    //删除文件
    @Test
    public void test_delete() throws Exception {

        RemoveObjectArgs removeObjectArgs = RemoveObjectArgs.builder()
                .bucket("testbucker")
                .object("壁纸.jpg") //对象名
                .build();
        //删除文件
        minioClient.removeObject(removeObjectArgs);
    }

    //查询文件 从minio中下载文件
    @Test
    public void test_getFile() throws Exception{
        GetObjectArgs getObjectArgs = GetObjectArgs.builder()
                .bucket("testbucker")
                .object("壁纸.jpg")
                .build();

        FilterInputStream inputStream = minioClient.getObject(getObjectArgs);

        //指定输出流
        FileOutputStream outputStream = new FileOutputStream(new File("D:\\桌面Desktop\\壁纸.jpg"));
        IOUtils.copy(inputStream,outputStream);

        //校验文件的完整性，对文件的内容进行md5
        String source_md5 = DigestUtils.md5Hex(inputStream); //minio中的文件MD5
        String local_md5 = DigestUtils.md5Hex(new FileInputStream(new File("D:\\桌面Desktop\\壁纸.jpg")));
        if(source_md5.equals(local_md5)){
            System.out.println("下载成功");
        }
    }

    //将分块文件上传到minio
    @Test
    public void uploadChunk() throws Exception {
        for (int i = 0; i < 9; i++) {
            //上传文件的参数信息
            UploadObjectArgs uploadObjectArgs = UploadObjectArgs.builder()
                    .bucket("testbucker") //确定桶
                    .filename("E:\\图片\\下载音乐\\chunk\\"+i) //指定本地文件路径
                    .object("chunk/"+i) //对象名
                    .build();
            //上传文件
            minioClient.uploadObject(uploadObjectArgs);
            System.out.println("上传分块"+i+"成功");
        }
    }

    //调用minio接口合并分块
    @Test
    public void testMerge() throws Exception{
//        List<ComposeSource> sources = null;
//        for (int i = 0; i < 9; i++) {
//            //指定分块文件的信息
//            ComposeSource composeSource = ComposeSource.builder()
//                    .bucket("testbucker")
//                    .object("chunk/" + i)
//                    .build();
//            sources.add(composeSource);
//        }
        List<ComposeSource> sources = Stream.iterate(0, i -> ++i)
                .limit(9)
                .map(i -> ComposeSource.builder()
                        .bucket("testbucket")
                        .object("chunk/".concat(Integer.toString(i)))
                        .build())
                .collect(Collectors.toList());

        //指定合并后的objectName等信息
        ComposeObjectArgs testbucker = ComposeObjectArgs.builder()
                .bucket("testbucker")
                .object("merge01.ogg")
                .build();
        //合并文件,minio默认的分块文件大小为5M
        minioClient.composeObject(testbucker);
    }
}
