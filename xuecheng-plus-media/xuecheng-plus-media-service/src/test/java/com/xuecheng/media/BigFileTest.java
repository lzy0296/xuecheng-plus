package com.xuecheng.media;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author: lzy
 * @create: 2024-03-19 10:31
 * @Description: 测试大文件上传方法
 * @version: v1.0
 */
public class BigFileTest {

    //将文件分块
    @Test
    public void testChunk() throws Exception {
        //源文件
        File sourceFile = new File("E:\\图片\\下载音乐\\春娇与志明 - 街道办GDC _ 欧阳耀莹.ogg");
        //分块文件存储路径
        String chunkFilePath = "E:\\图片\\下载音乐\\chunk\\";
        //分块文件的大小
        int chunkSize = 1024 * 1024 * 5;
        //分块文件个数
        int chunkNum = (int) Math.ceil(sourceFile.length() * 1.0 / chunkSize);
        //使用流从源文件读数据，像分块文件中写数据
        RandomAccessFile raf_r = new RandomAccessFile(sourceFile, "r");
        //缓存区
        byte[] bytes = new byte[1024];
        for(int i = 0; i < chunkNum; i++){
            File chunkFile = new File(chunkFilePath + i);
            //分块文件写入流
            RandomAccessFile raf_rw = new RandomAccessFile(chunkFile, "rw");
            int len = -1;

            while ((len = raf_r.read(bytes)) != -1){
                raf_rw.write(bytes,0,len);
                if(chunkFile.length() >= chunkSize){
                    break;
                }
            }
            raf_rw.close();
        }
        raf_r.close();
    }
    //将分块的文件合并
    @Test
    public void testMerge() throws Exception{
        //块文件目录
        File chunkFile = new File("E:\\图片\\下载音乐\\chunk\\");
        //源文件
        File sourceFile = new File("E:\\图片\\下载音乐\\春娇与志明 - 街道办GDC _ 欧阳耀莹.ogg");
        //合并后的文件
        File mergeFile = new File("E:\\图片\\下载音乐\\2_春娇与志明 - 街道办GDC _ 欧阳耀莹.ogg");

        //取出所有分块文件
        File[] files = chunkFile.listFiles();
        //将数组转成list
        List<File> fileList = Arrays.asList(files);
        //对分块文件排序
        Collections.sort(fileList, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return Integer.parseInt(o1.getName()) - Integer.parseInt(o2.getName());
            }
        });
        //向合并文件写的流
        RandomAccessFile raf_rw = new RandomAccessFile(mergeFile, "rw");
        //缓存区
        byte[] bytes = new byte[1024];

        //遍历分块文件，向合并的文件写
        for(File file : fileList) {
            //读分块的流
            RandomAccessFile raf_r = new RandomAccessFile(file, "r");
            int len = -1;
            while ((len=raf_r.read(bytes))!=-1){
                raf_rw.write(bytes,0,len);
            }
            raf_r.close();
        }
        raf_rw.close();
        //合并文件完成后对合并的文件md5校验
        FileInputStream fileInputStream_merge = new FileInputStream(mergeFile);
        FileInputStream fileInputStream_source = new FileInputStream(sourceFile);
        String md5_merge = DigestUtils.md5Hex(fileInputStream_merge);
        String md5_source = DigestUtils.md5Hex(fileInputStream_source);

        if(md5_merge.equals(md5_source)) System.out.println("文件合并成功");
    }
}
