package com.xuecheng.content.api;

import com.xuecheng.content.model.dto.BindTeachplanMediaDto;
import com.xuecheng.content.model.dto.SaveTeachplanDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.service.TechplanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: lzy
 * @create: 2024-03-15 17:48
 * @Description: 课程计划管理相关的接口
 * @version: v1.0
 */
@Api(value = "课程计划编辑接口",tags = "课程计划编辑接口")
@RestController
public class TeachplanController {

    @Autowired
    TechplanService techplanService;

    //查询课程计划
    @ApiOperation("查询课程计划树形结构")
    @GetMapping("/teachplan/{courseId}/tree-nodes")
    public List<TeachplanDto> getTreeNodes(@PathVariable Long courseId){
        List<TeachplanDto> teachplanTree = techplanService.findTeachplanTree(courseId);
        return teachplanTree;
    }

    @ApiOperation("课程计划创建或修改")
    @PostMapping("/teachplan")
    public void saveTeachplan( @RequestBody SaveTeachplanDto teachplan){
        techplanService.saveTeachplan(teachplan);
    }

    @ApiOperation("删除课程计划")
    @DeleteMapping("/teachplan/{id}")
    public void deleteTeachplan(@PathVariable Long id){
        techplanService.deleteTeachplan(id);
    }

    @ApiOperation("移动课程计划：movedown下移")
    @PostMapping("/teachplan/movedown/{id}")
    public List<TeachplanDto> moveTeachplan(@PathVariable Long id){
        List<TeachplanDto> teachplanDtos = techplanService.movedownTeachplan(id);
        return teachplanDtos;
    }

    @ApiOperation("移动课程计划：moveup上移")
    @PostMapping("/teachplan/moveup/{id}")
    public List<TeachplanDto> moveupTeachplan(@PathVariable Long id){
        List<TeachplanDto> teachplanDtos = techplanService.moveupTeachplan(id);
        return teachplanDtos;
    }

    @ApiOperation(value = "课程计划和媒资信息绑定")
    @PostMapping("/teachplan/association/media")
    public void associationMedia(@RequestBody BindTeachplanMediaDto bindTeachplanMediaDto){
        techplanService.associationMedia(bindTeachplanMediaDto);
    }

}
