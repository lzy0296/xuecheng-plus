package com.xuecheng.content.api;

import com.xuecheng.content.model.dto.AddCourseTeacherDto;
import com.xuecheng.content.model.dto.EditCourseTeacherDto;
import com.xuecheng.content.model.po.CourseTeacher;
import com.xuecheng.content.service.CourseTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author: lzy
 * @create: 2024-03-16 20:32
 * @Description: 师资管理相关接口
 * @version: v1.0
 */
@Api(value = "师资管理相关接口",tags = "师资管理相关接口")
@RestController
public class CourseTeacherController {

    @Autowired
    CourseTeacherService courseTeacherService;

    @ApiOperation("查询教师")
    @GetMapping("/courseTeacher/list/{courseId}")
    public List<CourseTeacher> getCourseTeacherById(@PathVariable Long courseId){
        List<CourseTeacher> courseTeacherById = courseTeacherService.getCourseTeacherById(courseId);
        return courseTeacherById;
    }

    @ApiOperation("添加教师")
    @PostMapping("/courseTeacher")
    public List<CourseTeacher> createCourseTeacher(@RequestBody @Validated AddCourseTeacherDto addCourseTeacherDto){
        List<CourseTeacher> courseTeacher = courseTeacherService.createCourseTeacher(addCourseTeacherDto);
        return courseTeacher;
    }

    @ApiOperation("修改教师")
    @PutMapping("/courseTeacher")
    public List<CourseTeacher> editCourseTeacher(@RequestBody @Validated EditCourseTeacherDto editCourseTeacherDto){
        List<CourseTeacher> courseTeacher = courseTeacherService.editCourseTeacher(editCourseTeacherDto);
        return courseTeacher;
    }

    @ApiOperation("删除教师")
    @DeleteMapping("/courseTeacher/course/{courseId}/{id}")
    public void moveCourseTeacher(@PathVariable Long courseId,@PathVariable Long id){
        courseTeacherService.moveCourseTeacher(courseId,id);
    }
}
