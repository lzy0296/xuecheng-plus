package com.xuecheng.content.model.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author: lzy
 * @create: 2024-03-16 20:54
 * @Description: 添加教师dto
 * @version: v1.0
 */
@Data
@ApiModel(value="AddCourseTeacherDto", description="新增教师基本信息")
public class AddCourseTeacherDto {

    @NotEmpty(message = "课程分类不能为空")
    @ApiModelProperty(value = "教师名称", required = true)
    private String teacherName;

    @ApiModelProperty(value = "课程id", required = true)
    private Long courseId;

    @NotEmpty(message = "课程分类不能为空")
    @ApiModelProperty(value = "教师职位", required = true)
    private String position;

    @ApiModelProperty(value = "教师简介", required = true)
    private String introduction;

    @ApiModelProperty(value = "教师照片", required = true)
    private String photograph;

}
