package com.xuecheng.content.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author: lzy
 * @create: 2024-03-16 21:22
 * @Description: 修改教师的数据模型
 * @version: v1.0
 */
@Data
public class EditCourseTeacherDto extends AddCourseTeacherDto{

    @ApiModelProperty(value = "教师id", required = true)
    private Long id;

    @ApiModelProperty(value = "创建时间", required = true)
    private LocalDateTime createDate;
}
