package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xuecheng.base.exception.XueChengPlusException;
import com.xuecheng.content.mapper.TeachplanMapper;
import com.xuecheng.content.mapper.TeachplanMediaMapper;
import com.xuecheng.content.model.dto.BindTeachplanMediaDto;
import com.xuecheng.content.model.dto.SaveTeachplanDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.model.po.Teachplan;
import com.xuecheng.content.model.po.TeachplanMedia;
import com.xuecheng.content.service.TechplanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: lzy
 * @create: 2024-03-15 18:38
 * @Description: 课程计划管理接口
 * @version: v1.0
 */
@Slf4j
@Service
public class TechplanServiceImpl implements TechplanService {
    @Autowired
    TeachplanMapper teachplanMapper;
    @Autowired
    TeachplanMediaMapper teachplanMediaMapper;


    @Override
    public List<TeachplanDto> findTeachplanTree(Long courseId) {
        List<TeachplanDto> teachplanDtos = teachplanMapper.selectTreeNodes(courseId);
        return teachplanDtos;
    }

    /**
     * @description 获取最新的排序号
     * @param courseId  课程id
     * @param parentId  父课程计划id
     * @return int 最新排序号
     * @author Mr.M
     * @date 2024/3/15 18:43
     */
    private int getTeachplanCount(Long courseId,Long parentId){
        LambdaQueryWrapper<Teachplan> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Teachplan::getCourseId,courseId).eq(Teachplan::getParentid,parentId);
        Integer count = teachplanMapper.selectCount(queryWrapper);
        return count+1;
    }
    @Override
    public void saveTeachplan(SaveTeachplanDto saveTeachplanDto) {
        //通过课程计划的id判断是新增还是修改
        Long teachplanId = saveTeachplanDto.getId();
        if(teachplanId == null){
            //新增
            Teachplan teachplan = new Teachplan();
            BeanUtils.copyProperties(saveTeachplanDto,teachplan);
            //确定排序字段，找到它的同级节点的个数，排序字段就是个数+1
            Long parentId = saveTeachplanDto.getParentid();
            Long courseId = saveTeachplanDto.getCourseId();
            int teachplanCount = getTeachplanCount(courseId, parentId);
            teachplan.setOrderby(teachplanCount);

            teachplanMapper.insert(teachplan);
        }else {
            //修改
            Teachplan teachplan = teachplanMapper.selectById(teachplanId);
            BeanUtils.copyProperties(saveTeachplanDto,teachplan);
            teachplanMapper.updateById(teachplan);
        }
    }

    @Transactional
    @Override
    public void deleteTeachplan(Long id) {
        //查询课程计划
        Teachplan teachplan = teachplanMapper.selectById(id);
        /**
         * 1、删除大章节，大章节下有小章节时不允许删除。
         * 2、删除大章节，大单节下没有小章节时可以正常删除。
         * 3、删除小章节，同时将关联的信息进行删除。
         **/
        //parentid = 0说明是大章节
        Long parentid = teachplan.getParentid();
        if(parentid == 0){
            //判断大章节下是否有小章节
            LambdaQueryWrapper<Teachplan> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(Teachplan::getParentid,id);
            List<Teachplan> teachplanList = teachplanMapper.selectList(queryWrapper);
            if(teachplanList != null && teachplanList.size() != 0){
                XueChengPlusException.cast("课程计划信息还有子级信息，无法操作");
            }
            int i = teachplanMapper.deleteById(id);
            if(i<=0){
                XueChengPlusException.cast("删除课程计划失败");
            }
        }else {
            //不等于0说明是小章节
            //删除课程计划
            int i = teachplanMapper.deleteById(id);
            if(i<=0){
                XueChengPlusException.cast("删除课程计划失败");
            }
            //删除TeachplanMedia媒资
            LambdaQueryWrapper<TeachplanMedia> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(TeachplanMedia::getTeachplanId,id);
            teachplanMediaMapper.delete(queryWrapper);
        }
    }

    @Transactional
    @Override
    public List<TeachplanDto> movedownTeachplan(Long id) {
        //查询课程计划
        Teachplan teachplan = teachplanMapper.selectById(id);
        //下移
        Teachplan teachplanNew = new Teachplan();
        BeanUtils.copyProperties(teachplan,teachplanNew);
        teachplanNew.setOrderby(teachplan.getOrderby()+1);//排序字段加1

        //更改下一个计划顺序-1，调换顺序
        LambdaQueryWrapper<Teachplan> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Teachplan::getParentid,teachplan.getParentid());
        queryWrapper.eq(Teachplan::getCourseId,teachplan.getCourseId());
        queryWrapper.eq(Teachplan::getOrderby,teachplan.getOrderby()+1);

        Teachplan teachplan1 = teachplanMapper.selectOne(queryWrapper);
        teachplan1.setOrderby(teachplan1.getOrderby()-1);
        //更改排序
        teachplanMapper.updateById(teachplanNew);
        teachplanMapper.updateById(teachplan1);

        //查询计划信息，返回结果
        List<TeachplanDto> teachplanTree = findTeachplanTree(id);
        return teachplanTree;
    }

    @Transactional
    @Override
    public List<TeachplanDto> moveupTeachplan(Long id) {
        //查询课程计划
        Teachplan teachplan = teachplanMapper.selectById(id);
        //上移
        Teachplan teachplanNew = new Teachplan();
        BeanUtils.copyProperties(teachplan,teachplanNew);
        teachplanNew.setOrderby(teachplan.getOrderby()-1);//排序字段减1

        //更改下一个计划顺序+1，调换顺序
        LambdaQueryWrapper<Teachplan> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Teachplan::getParentid,teachplan.getParentid());
        queryWrapper.eq(Teachplan::getCourseId,teachplan.getCourseId());
        queryWrapper.eq(Teachplan::getOrderby,teachplan.getOrderby()-1);
        Teachplan teachplan1 = teachplanMapper.selectOne(queryWrapper);
        teachplan1.setOrderby(teachplan1.getOrderby()+1);
        //更改排序
        teachplanMapper.updateById(teachplanNew);
        teachplanMapper.updateById(teachplan1);
        //查询计划信息，返回结果
        List<TeachplanDto> teachplanTree = findTeachplanTree(id);
        return teachplanTree;
    }

    @Transactional
    @Override
    public TeachplanMedia associationMedia(BindTeachplanMediaDto bindTeachplanMediaDto) {
        //教学计划id
        Long teachplanId = bindTeachplanMediaDto.getTeachplanId();
        Teachplan teachplan = teachplanMapper.selectById(teachplanId);
        if(teachplan==null){
            XueChengPlusException.cast("教学计划不存在");
        }
        Integer grade = teachplan.getGrade();
        if(grade!=2){
            XueChengPlusException.cast("只允许第二级教学计划绑定媒资文件");
        }
        //课程id
        Long courseId = teachplan.getCourseId();

        //先删除原来该教学计划绑定的媒资
        teachplanMediaMapper.delete(new LambdaQueryWrapper<TeachplanMedia>().eq(TeachplanMedia::getTeachplanId,teachplanId));

        //再添加教学计划与媒资的绑定关系
        TeachplanMedia teachplanMedia = new TeachplanMedia();
        teachplanMedia.setCourseId(courseId);
        teachplanMedia.setTeachplanId(teachplanId);
        teachplanMedia.setMediaFilename(bindTeachplanMediaDto.getFileName());
        teachplanMedia.setMediaId(bindTeachplanMediaDto.getMediaId());
        teachplanMedia.setCreateDate(LocalDateTime.now());
        teachplanMediaMapper.insert(teachplanMedia);
        return teachplanMedia;
    }

}
