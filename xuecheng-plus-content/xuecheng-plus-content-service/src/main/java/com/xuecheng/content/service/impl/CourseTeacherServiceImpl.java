package com.xuecheng.content.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xuecheng.base.exception.XueChengPlusException;
import com.xuecheng.content.mapper.CourseTeacherMapper;
import com.xuecheng.content.model.dto.AddCourseTeacherDto;
import com.xuecheng.content.model.dto.EditCourseTeacherDto;
import com.xuecheng.content.model.po.CourseTeacher;
import com.xuecheng.content.service.CourseTeacherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author: lzy
 * @create: 2024-03-16 20:41
 * @Description: 师资信息管理接口实现类
 * @version: v1.0
 */
@Slf4j
@Service
public class CourseTeacherServiceImpl implements CourseTeacherService {

    @Autowired
    CourseTeacherMapper courseTeacherMapper;

    @Override
    public List<CourseTeacher> getCourseTeacherById(Long courseId) {
        LambdaQueryWrapper<CourseTeacher> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CourseTeacher::getCourseId,courseId);
        List<CourseTeacher> courseTeachers = courseTeacherMapper.selectList(queryWrapper);
        return courseTeachers;
    }

    @Override
    public List<CourseTeacher> createCourseTeacher(AddCourseTeacherDto addCourseTeacherDto) {
        //插入数据
        CourseTeacher courseTeacher = new CourseTeacher();
        BeanUtils.copyProperties(addCourseTeacherDto,courseTeacher);
        courseTeacher.setCreateDate(LocalDateTime.now());
        int i = courseTeacherMapper.insert(courseTeacher);
        if(i<=0){
            XueChengPlusException.cast("新增教师基本信息失败");
        }
        //查询数据并返回
        Long id = courseTeacher.getCourseId();
        List<CourseTeacher> courseTeachers = getCourseTeacherById(id);
        return courseTeachers;
    }

    @Override
    public List<CourseTeacher> editCourseTeacher(EditCourseTeacherDto editCourseTeacherDto) {
        CourseTeacher courseTeacher = new CourseTeacher();
        BeanUtils.copyProperties(editCourseTeacherDto,courseTeacher);
        int i = courseTeacherMapper.updateById(courseTeacher);
        if(i<=0){
            XueChengPlusException.cast("修改教师基本信息失败");
        }
        List<CourseTeacher> courseTeacherById = getCourseTeacherById(courseTeacher.getCourseId());
        return courseTeacherById;
    }

    @Override
    public void moveCourseTeacher(Long courseId, Long id) {
        LambdaQueryWrapper<CourseTeacher> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CourseTeacher::getCourseId,courseId);
        queryWrapper.eq(CourseTeacher::getId,id);
        int i = courseTeacherMapper.delete(queryWrapper);
        if(i<=0) XueChengPlusException.cast("删除教师信息失败");
    }
}
