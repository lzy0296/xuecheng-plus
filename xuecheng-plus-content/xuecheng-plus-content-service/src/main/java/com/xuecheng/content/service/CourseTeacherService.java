package com.xuecheng.content.service;

import com.xuecheng.content.model.dto.AddCourseTeacherDto;
import com.xuecheng.content.model.dto.EditCourseTeacherDto;
import com.xuecheng.content.model.po.CourseTeacher;

import java.util.List;

/**
 * @author: lzy
 * @create: 2024-03-13 13:22
 * @Description: 师资信息管理接口
 * @version: v1.0
 */
public interface CourseTeacherService {

    /**
     * 查询教师
     * @param courseId 课程id
     * @return
     */
    public List<CourseTeacher> getCourseTeacherById(Long courseId);

    /**
     * 添加教师
     * @param addCourseTeacherDto
     * @return
     */
    public List<CourseTeacher> createCourseTeacher(AddCourseTeacherDto addCourseTeacherDto);

    /**
     * 修改教师
     * @param editCourseTeacherDto
     * @return
     */
    public List<CourseTeacher> editCourseTeacher(EditCourseTeacherDto editCourseTeacherDto);

    /**
     * 删除教师
     * @param courseId
     * @param id
     */
    public void moveCourseTeacher(Long courseId,Long id);
}
