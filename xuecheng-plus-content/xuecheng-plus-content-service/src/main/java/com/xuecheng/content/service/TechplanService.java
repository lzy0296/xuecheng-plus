package com.xuecheng.content.service;

import com.xuecheng.content.model.dto.BindTeachplanMediaDto;
import com.xuecheng.content.model.dto.SaveTeachplanDto;
import com.xuecheng.content.model.dto.TeachplanDto;
import com.xuecheng.content.model.po.TeachplanMedia;

import java.util.List;

/**
 * @author: lzy
 * @create: 2024-03-13 13:22
 * @Description: 课程计划管理接口
 * @version: v1.0
 */
public interface TechplanService {

    /**
     * 根据课程id查询课程计划
     * @param courseId 课程id
     * @return
     */
    public List<TeachplanDto> findTeachplanTree(Long courseId);

    /**
     * 新增/修改/保存课程计划
     * @param saveTeachplanDto
     */
    public void saveTeachplan(SaveTeachplanDto saveTeachplanDto);

    /**
     * 删除课程计划
     * @param id 课程计划id
     */
    public void deleteTeachplan(Long id);

    /**
     * 移动课程计划 下移
     * @param id 课程计划id
     * @return 课程计划信息
     */
    public List<TeachplanDto> movedownTeachplan(Long id);
    /**
     * 移动课程计划 上移
     * @param id 课程计划id
     * @return 课程计划信息
     */
    public List<TeachplanDto> moveupTeachplan(Long id);

    /**
     * 教学计划绑定媒资
     * @param bindTeachplanMediaDto
     * @return TeachplanMedia
     */
    public TeachplanMedia associationMedia(BindTeachplanMediaDto bindTeachplanMediaDto);
}
